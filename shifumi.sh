#!/bin/bash
echo "C'est le shifumi, selectionne 1,2 ou 3 pour jouer contre l'IA"
#pierre=1
#feuille=2
#ciseaux=3
#r=$(( ( RANDOM % 3 )  + 1 ))

OPTIONS="Pierre Feuille Ciseaux Quitter"
select opt in $OPTIONS; do
    if [ "$opt" = "Pierre" ]; then
        j=$(( ( RANDOM % 3 )  + 2 ));
        if (( $j==2 )); then #Pierre+Pierre=2
            echo "Joueur: Pierre / IA: Pierre donc Match nul";
        elif (( $j==3 )); then #Pierre+Feuille=3
            echo "Joueur: Pierre / IA: Feuille donc Joueur Lose";
        elif (( $j==4 )); then #Pierre+Ciseaux=4
            echo "Joueur: Pierre / IA: Ciseaux donc Joueur Win";
        fi

    elif [ "$opt" = "Feuille" ]; then
        j=$(( ( RANDOM % 3 )  + 3 ));
        if (( $j==3 )); then #Feuille+Pierre=3 
            echo "Joueur: Feuille / IA: Pierre donc Joueur Win";
        elif (( $j==4 )); then #Feuille+Feuille=4 
            echo "Joueur: Feuille / IA: Feuille donc Joueur Match nul";
        elif (( $j==5 )); then #Feuille+Ciseaux=5
            echo "Joueur: Feuille / IA: Ciseaux donc Joueur Lose";
        fi

    elif [ "$opt" = "Ciseaux" ]; then
        j=$(( ( RANDOM % 3 )  + 4 ));
        if (( $j==4 )); then #Ciseaux+Pierre=4
            echo "Joueur: Ciseaux / IA: Pierre donc Joueur Lose";
        elif (( $j==5 )); then #Ciseaux+Feuille=5
            echo "Joueur: Ciseaux / IA: Feuille donc Joueur Win";
        elif (( $j==6 )); then #Ciseaux+Ciseaux=6
            echo "Joueur: Ciseaux / IA: Ciseaux donc Match nul";
        fi
    elif [ "$opt" = "Quitter" ]; then
    echo done
    exit

    else
        clear
        echo "Juste 1,2 ou 3 svp, vous ne faites pas d'effort"
    fi
done


